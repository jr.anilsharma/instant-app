package com.demo.anil.appmodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;

import com.demo.anil.tictactoe.GameActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText etEmail, etPassword;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        initListeners();
    }


    /**
     *
     */
    private void initView() {
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    /**
     *
     */
    private void initListeners() {
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                moveToGame();
                break;
            default:
                break;
        }
    }


    private void moveToGame() {
        startActivity(new Intent(this, GameActivity.class));
    }

}
