package com.demo.anil.appmodule;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class FinishActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        findViewById(R.id.btnKill).setOnClickListener(v -> finishAffinity());
    }
}
