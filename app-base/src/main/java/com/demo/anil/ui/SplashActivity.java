package com.demo.anil.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;


public class SplashActivity extends AppCompatActivity {

    private TextView tvCounter;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tvCounter = findViewById(R.id.counter);
        new CountDownTimer(4000, 1000) {
            public void onFinish() {
                //moveToGame();
                moveToLogin();
            }

            public void onTick(long millisUntilFinished) {
                switch (counter) {
                    case 0:
                        tvCounter.setText("Ready");
                        break;
                    case 1:
                        tvCounter.setText("Set");
                        break;
                    case 2:
                        tvCounter.setText("Go");
                        break;
                    default:
                        break;
                }
                counter++;
                Log.e("tick", "tick " + counter);
            }
        }.start();
    }

    private void moveToGame() {
        startActivity(new Intent(this, GameActivity.class));
        finish();
    }


    private void moveToLogin() {
        try {
            startActivity(new Intent(this, Class.forName("com.demo.anil.appmodule.LoginActivity")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finish();
    }
}
