package com.demo.anil.tictactoe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class ExtraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra);

        findViewById(R.id.btnLastScreen).setOnClickListener(v -> {
            try {
                Intent i = new Intent(this,
                        Class.forName("com.demo.anil.appmodule.FinishActivity"));
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            } catch (ClassNotFoundException e) {
                showDownloadMessage();
                e.printStackTrace();
            }
        });

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }


    private void showDownloadMessage() {
        Log.e("showDownloadMessage function", "called");
//        AlertDialog dialog = new AlertDialog.Builder(getApplicationContext()).setMessage("Please download app").create();
//        dialog.show();

    }

}
